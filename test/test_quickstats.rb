#!/usr/bin/env ruby -w

require 'test/unit'
require_relative '../lib/quickstats.rb'

class QuickStats_test < Test::Unit::TestCase
  def test_quickstats
    qs = QuickStats.new
    assert_equal 0, qs.n
    assert_true qs.sample_mean.nan?
    assert_true qs.sample_variance.nan?
    assert_true qs.min.nan?
    assert_true qs.max.nan?
    assert_true qs.loss(target: 0).nan?

    qs.add_set [41,43]
    assert_equal 2, qs.n
    assert_equal 42.0, qs.sample_mean
    assert_equal 2.0, qs.sample_variance
    assert_equal Math.sqrt(2.0), qs.standard_deviation
    assert_equal 41.0, qs.min
    assert_equal 43.0, qs.max
    assert_equal 1.0, qs.standard_error
    assert_equal 1.0, qs.loss(target: 42.0)
    assert_equal 2.0, qs.loss(target: 43.0)
    assert_equal 65.0, qs.loss(target: 50.0)
    qs.new_obs 42
    assert_equal 3, qs.n
    assert_equal 42.0, qs.sample_mean
    assert_equal 1.0, qs.sample_variance
    assert_equal 1.0, qs.standard_deviation
    assert_equal 41.0, qs.min
    assert_equal 43.0, qs.max
    assert_equal 2.0/3.0, qs.loss(target: 42)
    assert_equal (1.0 + 2.0/3.0), qs.loss(target: 43)

    qs.reset
    assert_equal 0, qs.n
    assert_true qs.sample_mean.nan?
    assert_true qs.sample_variance.nan?
    assert_true qs.min.nan?
    assert_true qs.max.nan?
    assert_true qs.loss(target: 0).nan?
  end

  def test_aliases
    qs = QuickStats.new
    qs.add_all [41,43]

    assert_equal qs.n, qs.sample_size
    assert_equal qs.sample_mean, qs.average
    assert_equal qs.sample_mean, qs.avg
    assert_equal qs.sample_variance, qs.var
    assert_equal qs.standard_deviation, qs.std_dev
    assert_equal qs.standard_error, qs.std_err
  end

end
