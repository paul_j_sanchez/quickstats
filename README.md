A Ruby gem which provides basic statistical functionality,
including sample mean, sample variance, standard deviation,
standard error, min, max, sample size, and evaluation of
quadratic loss relative to a target value.

See https://www.rubydoc.info/gems/quickstats/ for
full documentation.
