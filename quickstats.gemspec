# -*- ruby -*-
_VERSION = "2.2.2"

Gem::Specification.new do |s|
  s.name = "quickstats"
  s.version = _VERSION
  s.date = "2020-05-01"
  s.summary = "Computationally stable and efficient basic descriptive statistics."
  s.email = "pjs@alum.mit.edu"
  s.description = "This class uses Kalman Filter updating to tally sample mean and sum of squared deviations from the average, along with min, max, and sample size. Sample variance, standard deviation, and standard error are calculated on demand."
  s.author = "Paul J Sanchez"
  s.files = %w[
    quickstats.gemspec
    LICENSE
    lib/quickstats.rb
    Rakefile
    README.md
    test/test_quickstats.rb
  ]
  s.required_ruby_version = '>= 2.3'
  s.homepage = 'https://bitbucket.org/paul_j_sanchez/quickstats'
  s.license = 'MIT'
end
