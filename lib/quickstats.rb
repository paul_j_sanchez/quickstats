#!/usr/bin/env ruby -w

# Computationally stable and efficient basic descriptive statistics.
# This class uses Kalman Filter updating to tally sample mean and sum
# of squares, along with min, max, and sample size. Sample variance,
# standard deviation and standard error are calculated on demand.
#
# Author:: Paul J Sanchez (mailto:pjs@alum.mit.edu)
# Copyright:: Copyright (c) Paul J Sanchez
# License:: MIT
#
class QuickStats
  attr_reader :n, :sample_mean, :min, :max, :ssd

  alias average sample_mean
  alias avg sample_mean
  alias sample_size n
  alias sum_squared_deviations ssd

  # Initialize state vars in a new QuickStats object.
  #
  # - n = 0
  # - sample_mean = sample_variance = min = max = NaN
  #
  def initialize
    reset
  end

  # Reset all state vars to initial values.
  #
  # - ssd = n = 0
  # - sample_mean = sample_variance = min = max = NaN
  #
  # *Returns*::
  #   - a reference to the QuickStats object.
  def reset
    @ssd = @n = 0
    @sample_mean = @max = @min = Float::NAN
    self
  end

  # Update the sample size, sample mean, sum of squares, min, and max given
  # a new observation. All but the sample size are maintained as floating point.
  #
  # *Arguments*::
  #   - +datum+ -> the new observation.  Must be numeric
  # *Returns*::
  #   - a reference to the QuickStats object.
  # *Raises*::
  #   - RuntimeError if datum is non-numeric
  #
  def new_obs(datum)
    fail 'Observations must be numeric' unless datum.is_a? Numeric
    x = datum.to_f
    @max = x unless x <= @max
    @min = x unless x >= @min
    if @n > 0
      delta = x - @sample_mean
      @n += 1
      @sample_mean += delta / n
      @ssd += delta * (x - @sample_mean)
    else
      @sample_mean = x
      @n += 1
    end
    self
  end

  # Update the statistics with all elements of an enumerable set.
  #
  # *Arguments*::
  #   - +enumerable_set+ -> the set of new observation.  All elements
  #     must be numeric.
  # *Returns*::
  #   - a reference to the QuickStats object.
  #
  def add_set(enumerable_set)
    enumerable_set.each { |x| new_obs x }
    self
  end

  alias add_all add_set

  # Calculates the unbiased sample variance on demand (divisor is n-1).
  #
  # *Returns*::
  #   - the sample variance of the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def sample_variance
    @n > 1 ? @ssd / (@n - 1) : Float::NAN
  end

  alias var sample_variance

  # Calculates the MLE sample variance on demand (divisor is n).
  #
  # *Returns*::
  #   - the MLE sample variance of the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def mle_sample_variance
    @n > 1 ? @ssd / @n : Float::NAN
  end

  alias mle_var mle_sample_variance

  # Calculates the square root of the unbiased sample variance on demand.
  #
  # *Returns*::
  #   - the sample standard deviation of the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def standard_deviation
    Math.sqrt sample_variance
  end

  alias s standard_deviation
  alias std_dev standard_deviation

  # Calculates the square root of the MLE sample variance on demand.
  #
  # *Returns*::
  #   - the MLE standard deviation of the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def mle_standard_deviation
    Math.sqrt mle_sample_variance
  end

  alias mle_s mle_standard_deviation
  alias mle_std_dev mle_standard_deviation

  # Calculates sqrt(sample_variance / n) on demand.
  #
  # *Returns*::
  #   - the sample standard error of the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def standard_error
    Math.sqrt(sample_variance / @n)
  end

  alias std_err standard_error

  # Calculates sqrt(mle_sample_variance / n) on demand.
  #
  # *Returns*::
  #   - the sample standard error of the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def mle_standard_error
    Math.sqrt(mle_sample_variance / @n)
  end

  alias mle_std_err mle_standard_error

  # Estimates of quadratic loss (a la Taguchi) relative to a specified
  # target value.
  #
  # *Arguments*::
  #   - +target:+ -> the designated target value for the loss function.
  # *Returns*::
  #   - the quadratic loss calculated for the data, or NaN if this is a
  #     new or just-reset QuickStats object.
  def loss(target:)
    fail 'Must supply target to loss function' unless target
    @n > 1 ? (@sample_mean - target)**2 + @ssd / @n : Float::NAN
  end
end
